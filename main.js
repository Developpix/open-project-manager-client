const { app, BrowserWindow, Menu, shell, Tray } = require("electron");

let window;

const createWindow = () => {
  // Create the window
  window = new BrowserWindow({
    title: "Open Project Manager",
    darkTheme: true
  });

  // Create application's menu
  const menu = Menu.buildFromTemplate([
    {
      label: "File",
      submenu: [{ type: "separator" }, { role: "quit" }]
    },
    {
      label: "Edit",
      submenu: [
        { role: "undo" },
        { role: "redo" },
        { type: "separator" },
        { role: "cut" },
        { role: "copy" },
        { role: "paste" },
        { role: "pasteandmatchstyle" },
        { role: "delete" },
        { role: "selectall" }
      ]
    },
    {
      label: "View",
      submenu: [
        { role: "reload" },
        { role: "forcereload" },
        { role: "toggledevtools" },
        { type: "separator" },
        { role: "resetzoom" },
        { role: "zoomin" },
        { role: "zoomout" },
        { type: "separator" },
        { role: "togglefullscreen" }
      ]
    },
    {
      label: "Help",
      submenu: [
        {
          label: "About",
          click() {
            shell.openExternal(
              "https://gitlab.com/Developpix/open-project-manager-client"
            );
          }
        }
      ]
    }
  ]);
  Menu.setApplicationMenu(menu);

  // Load welcome page
  window.loadFile("views/index.html");

  // On window closed delete object
  window.on("closed", () => {
    window = null;
  });
};

/**
 * Function to show or hide the window according to if it was visible or not
 */
const showWindow = () => {
  if (window == null) createWindow();
  else if (!window.isVisible()) window.show();
  else window.hide();
};

// When the application is ready
app.on("ready", () => {
  // Define application name
  app.setName("Open Project Manager");

  // Create a tray icon
  let tray = new Tray(`${__dirname}/icon.png`);

  // Create the tray icon menu
  const menu = Menu.buildFromTemplate([
    {
      label: "Show",
      click: showWindow
    },
    { type: "separator" },
    { role: "quit" }
  ]);
  tray.setContextMenu(menu);

  // Define a tool tip text for the icon 
  tray.setToolTip("Open Project Manager");
  
  // Create the window
  createWindow();
  
    // On click on the icon show or hide the window
    tray.on("click", showWindow);
});

// If all window are closed don't close the application, tray icon stay
app.on("window-all-closed", e => {
  e.preventDefault();
});
