# Commits

## Messages structure
```
<type>(<impact>): <subject>
<EMPTY LINE>
<body>
<EMPTY LINE>
<footer>
```

## Type
Type can be one or many (separated by `,`) type contained in the list below :

* **build**: change compilation or dependencies
* **ci**: CI/CD modifications
* **doc**: write documentation
* **feat**: new features
* **fix**: fix bugs
* **perf**: increase performance
* **ref**: code modifications which doesn't product a new features or fixes a bug
* **style**: code modifications that doesn't affect program execution
* **test**: new tests

## Impact

The part of the application affected by your modifications.

## Subject

Short description of your modifications.

## Corps

Here you can describe your modifications in detail.

## Footer

You can use footer for close issue, assign label to an issue, etc.